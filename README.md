# blackjack

This is a simple Blackjack game played via the CLI.

Some notes:
1. only 1 deck is used
2. if the deck runs out of cards in the middle of a round, the discarded cards are reshuffled and used


**Requirements**

- Maven 3.x
- Java 1.8



**To run**

`mvn clean compile exec:java -Dexec.mainClass="org.gamboa.blackjack.BlackjackGame"`

alternatively

create a JAR `mvn clean package` and run it `java -jar target/blackjack-1.0-SNAPSHOT.jar`
