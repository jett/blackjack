package org.gamboa.blackjack.ui;

public interface UserInterface {

    void showMessage(String... messages);
    String getResponse();

}
