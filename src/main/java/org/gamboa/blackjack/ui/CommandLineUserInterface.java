package org.gamboa.blackjack.ui;

import java.util.Scanner;

public class CommandLineUserInterface implements UserInterface {

    @Override
    public void showMessage(String... messages) {
        for (String message : messages) {
            System.out.print(message);
        }
        System.out.println("");
    }

    @Override
    public String getResponse() {
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
}
