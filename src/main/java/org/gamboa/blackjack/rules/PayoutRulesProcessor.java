package org.gamboa.blackjack.rules;

import org.gamboa.blackjack.Hand;

import java.math.BigDecimal;

public interface PayoutRulesProcessor {

    BigDecimal assessHands(Hand dealerHand, Hand playerHand);

}
