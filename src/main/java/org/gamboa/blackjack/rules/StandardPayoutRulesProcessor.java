package org.gamboa.blackjack.rules;

import java.math.BigDecimal;

import org.gamboa.blackjack.Hand;


public class StandardPayoutRulesProcessor implements PayoutRulesProcessor {

    @Override
    public BigDecimal assessHands(Hand dealerHand, Hand playerHand) {

        BigDecimal factor;

        if (playerHand.isNaturalBlackjack()) {

            if (dealerHand.isNaturalBlackjack()) {
                factor = BigDecimal.ONE;
            } else {
                factor = BigDecimal.valueOf(2.5);
            }

        } else if (playerHand.surrendered()) {

            // if the hand was surrendered, we return half of what was wagered
            factor = BigDecimal.valueOf(0.5);

        } else if (playerHand.isBusted()) {

            // if player is busted we lose the wager (even if dealer is also busted)
            factor = BigDecimal.ONE.negate();

        } else if (dealerHand.isBusted()) {

            if (playerHand.isBusted()) {
                factor = BigDecimal.ONE.negate();
            } else {
                factor = BigDecimal.valueOf(2);
            }

        } else {

            // no one was busted, or surrendered
            if (dealerHand.getValue() == playerHand.getValue()) {

                // it's a tie, we return the wager
                factor = BigDecimal.ONE;

            } else if (dealerHand.getValue() > playerHand.getValue()) {

                // dealer wins, we lose the wwager
                factor = BigDecimal.valueOf(-1);

            } else {

                // player won
                factor = BigDecimal.valueOf(2);
            }
        }

        return playerHand.getWager().multiply(factor);
    }
}
