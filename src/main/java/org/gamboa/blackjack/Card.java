package org.gamboa.blackjack;

public class Card {

    public enum Suit {
        HEART("h"),
        SPADE("s"),
        DIAMOND("d"),
        CLUB("c");

        private final String name;

        Suit(String s) {
            name = s;
        }


        @Override
        public String toString() {
            return this.name;
        }
    }

    public enum Face {
            JACK("J"),
            QUEEN("Q"),
            KING("K");

            private final String name;

            Face(String s) {
                name = s;
            }

            @Override
            public String toString() {
                return this.name;
            }
    }

    static final String ACE = "A";

    private String faceValue;
    private Suit suit;
    private Integer value;
    private Boolean faceUp;

    Card(String faceValue, Suit suit, Integer value) {
        this.faceValue = faceValue;
        this.suit = suit;
        this.value = value;
        this.faceUp = true;
    }

    Integer getValue() {
        return value;
    }

    boolean isAce() {
        return this.faceValue.equals(ACE);
    }

    Card faceDown() {
        this.faceUp = false;
        return this;
    }

    Card flipUp() {
        this.faceUp = true;
        return this;
    }

    boolean hasSameRank(Card card) {
        return this.faceValue.equals(card.faceValue);
    }

    @Override
    public String toString() {
        if (this.faceUp) {
            return this.faceValue + this.suit;
        } else {
            return "**";
        }
    }
}
