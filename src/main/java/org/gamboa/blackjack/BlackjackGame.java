package org.gamboa.blackjack;

import org.gamboa.blackjack.rules.PayoutRulesProcessor;
import org.gamboa.blackjack.rules.StandardPayoutRulesProcessor;
import org.gamboa.blackjack.ui.CommandLineUserInterface;
import org.gamboa.blackjack.ui.UserInterface;

public class BlackjackGame {

    BlackjackGame() {
    }

    void start() {
        Deck deck = new Deck();

        UserInterface clui = new CommandLineUserInterface();
        PayoutRulesProcessor rulesProcessor = new StandardPayoutRulesProcessor();
        Player player = new Player("Player 1");

        Dealer dealer = new Dealer(clui, rulesProcessor, deck, player);
        dealer.play();
    }

    public static void main(String args[]) {
        BlackjackGame game = new BlackjackGame();
        game.start();
    }
}
