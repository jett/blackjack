package org.gamboa.blackjack;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/***
 *
 */
class Player {

    private String name;
    private BigDecimal money;
    private List<Hand> handsList;

    static final BigDecimal DEFAULT_WAGER = BigDecimal.valueOf(100);

    Player(String name) {
        this.name = name;
        handsList = new ArrayList<>();
        money = BigDecimal.valueOf(100);
    }

    String getName() {
        return name;
    }

    void addHand(Hand hand) {
        handsList.add(hand);
    }

    void charge(BigDecimal amount) {
        money = this.money.subtract(amount);
    }

    void pay(BigDecimal amount) {
        money = this.money.add(amount);
    }

    List<Hand> getAllHands() {
        return handsList;
    }

    Optional<Hand> getNextInPlayHand() {
        return handsList.stream().filter(hand -> hand.inPlay()).findFirst();
    }

    Boolean hasMoney() {
        return money.compareTo(BigDecimal.valueOf(10)) >= 0;
    }

    BigDecimal getMoney() {
        return money;
    }

    void clearHands() {
        handsList.clear();
    }

    Boolean gotBlackJack() {
        return handsList.size() == 1 && handsList.get(0).isNaturalBlackjack();
    }
}
