package org.gamboa.blackjack;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// a hand represents the cards held by a player
public class Hand {

    public enum HandStatus { INPLAY, STAND, BUSTED, SURRENDERED, NATURAL }

    private BigDecimal wager = BigDecimal.ZERO;
    private Integer value = 0;
    private List<Card> cards = new ArrayList<>();
    private Boolean stand;
    private HandStatus status = HandStatus.INPLAY;


    public Integer getValue() {
        return this.value;
    }

    void addWager(BigDecimal amount) {
        this.wager = this.wager.add(amount);
    }

    void addCard(Card card) {
        this.cards.add(card);
        this.value = recomputeHandValue();

        if (value > 21) {
            status = HandStatus.BUSTED;
        }

        if (cards.size() == 2 && value == 21) {
            status = HandStatus.NATURAL;
        }
    }

    Boolean isSplittable() {
        return cards.size() == 2 && cards.get(0).hasSameRank(cards.get(1));
    }

    public Boolean surrendered() {
        return status == HandStatus.SURRENDERED;
    }

    public Boolean isBusted() {
        return status == HandStatus.BUSTED;
    }

    public Boolean isNaturalBlackjack() {
        return status == HandStatus.NATURAL;
    }

    Boolean inPlay() {
        return status == HandStatus.INPLAY;
    }

    Hand split() {
        // splits the hand into two hands
        Hand otherHand = new Hand();

        Card removedCard = cards.remove(1);
        this.value = recomputeHandValue();
        otherHand.addCard(removedCard);

        return otherHand;
    }

    void stand() {
        this.status = HandStatus.STAND;
    }

    void surrender() {
        this.status = HandStatus.SURRENDERED;
    }

    void flipAllUp() {
        for (Card card : cards) {
            card.flipUp();
        }
    }

    HandStatus getStatus() {
        return status;
    }

    public BigDecimal getWager() {
        return wager;
    }

    private Integer recomputeHandValue() {
        Integer newValue = 0;
        for (Card c : cards) {
            if (c.isAce()) {
                newValue += (newValue+ c.getValue()) > 21 ? 1 : c.getValue();
            } else {
                newValue += c.getValue();
            }
        }
        return newValue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Card card : cards) {
            sb.append(card);
            sb.append("  ");
        }

        return sb.toString();
    }
}
