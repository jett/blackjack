package org.gamboa.blackjack;

import java.math.BigDecimal;

import org.gamboa.blackjack.rules.PayoutRulesProcessor;
import org.gamboa.blackjack.ui.UserInterface;

class Dealer {

    private static final BigDecimal DEFAULT_BET = BigDecimal.TEN;

    private UserInterface ui;
    private PayoutRulesProcessor rulesProcessor;
    private Hand dealerHand;
    private Deck deck;
    private Player player;


    Dealer(UserInterface ui, PayoutRulesProcessor rulesProcessor, Deck deck, Player player) {
        this.ui = ui;
        this.rulesProcessor = rulesProcessor;
        this.deck = deck;
        this.player = player;
    }


    void play() {

        Boolean continuePlaying = true;

        // continue playing rounds until player says no or runs out of money
        while (continuePlaying && player.hasMoney()) {

            playRound();

            if (!player.hasMoney()) {
                ui.showMessage("Sorry, you do not have enough money to play another round");
            } else {
                ui.showMessage("");
                ui.showMessage(String.format("You have %.0f. Do you want to play another round? (y/other)",
                        player.getMoney()));
                String response = ui.getResponse();

                if (!response.equals("y")) {
                    continuePlaying = false;
                } else {
                    player.clearHands();
                    dealerHand = null;
                }
            }
        }
    }


    private void playRound() {

        initialRoundDeal();

        if (player.gotBlackJack()) {
            ui.showMessage("You got a Blackjack!!! ");
        }

        // player makes decisions on his hand(s)
        while (player.getNextInPlayHand().isPresent()) {

            Hand hand = player.getNextInPlayHand().get();   // NOSONAR
            String playerOptions = buildHandOptions(hand);

            ui.showMessage(String.format("%s hand: ", player.getName()), hand.toString());
            ui.showMessage(playerOptions);
            String response = ui.getResponse();

            processPlayerAction(response, player, hand);
        }

        // The player is done playing, dealer plays his hand
        playOwnHand();

        showSummary();

    }


    private void initialRoundDeal() {
        Hand hand = new Hand();
        hand.addCard(deck.draw());
        hand.addCard(deck.draw());
        hand.addWager(DEFAULT_BET);

        player.charge(DEFAULT_BET);
        player.addHand(hand);

        // deal 2 cards for the dealer with the second face down
        dealerHand = new Hand();
        dealerHand.addCard(deck.draw());
        dealerHand.addCard(deck.draw().faceDown());

        ui.showMessage("Dealer's Hand: ", dealerHand.toString());
        ui.showMessage("");
    }


    private void playOwnHand() {
        ui.showMessage("");
        ui.showMessage("Dealer playing own hand ... ");

        // dealer keeps on drawing until he hits > 17
        while (dealerHand.getValue() < 17) {
            dealerHand.addCard(deck.draw());
        }
    }


    private void showSummary() {

        // show dealer and player hands
        dealerHand.flipAllUp();
        ui.showMessage("");
        ui.showMessage("Round Results");
        ui.showMessage("Dealer's final hand: ", dealerHand.toString());

        for(Hand hand : player.getAllHands()) {

            ui.showMessage(String.format("%s hand #: ", player.getName()), hand.toString());

            // check if this hand won/loss
            BigDecimal gainsOrLoss = rulesProcessor.assessHands(dealerHand, hand);

            if (gainsOrLoss.compareTo(BigDecimal.ZERO) > 0) {

                ui.showMessage(String.format("Player won/recovered %.0f in this hand.",
                        gainsOrLoss.abs()));

                // pay the player his earnings
                player.pay(gainsOrLoss);

            } else {

                ui.showMessage(String.format("Player lost %.0f in this hand.",
                        gainsOrLoss.abs()));

            }
        }
    }


    private void processPlayerAction(String action, Player player, Hand hand) {

        switch (action) {
            case "1":
                hit(hand);
                break;
            case "2":
                stand(hand);
                break;
            case "3":
                doubleDown(player, hand);
                break;
            case "4":
                split(player, hand);
                break;
            case "5":
                surrender(hand);
                break;
            default:
                ui.showMessage("Invalid action selected");
                break;
        }
    }


    // build a list of options available for the current hand
    private String buildHandOptions(Hand hand) {
        StringBuilder sb = new StringBuilder();

        if(hand.getValue() < 21) {
            sb.append("[1] Hit ");
        }

        sb.append("[2] Stand ");
        sb.append("[3] Double Down ");

        if (hand.isSplittable()) {
            sb.append("[4] Split ");
        }

        sb.append("[5] Surrender ");
        return sb.toString();
    }


    private void split(Player player, Hand hand) {
        Hand newHand = hand.split();
        newHand.addCard(deck.draw());
        newHand.addWager(hand.getWager());

        player.charge(newHand.getWager());

        player.addHand(newHand);

        hand.addCard(deck.draw());
    }


    private void hit(Hand hand) {
        Card c = deck.draw();
        hand.addCard(c);
    }


    private void stand(Hand hand) {
        hand.stand();
    }


    private void doubleDown(Player player, Hand hand) {
        Card c = deck.draw();
        hand.addCard(c);

        hand.addWager(hand.getWager());
        player.charge(hand.getWager());

        if (!hand.isBusted()) {
            hand.stand();
        }
    }


    private void surrender(Hand hand) {
        hand.surrender();
    }
}
