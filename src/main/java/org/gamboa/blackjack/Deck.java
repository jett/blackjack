package org.gamboa.blackjack;


import java.util.*;

class Deck {

    private Queue<Card> cardStack = new LinkedList<>();
    private List<Card> discardPile = new ArrayList<>();

    Deck() {

        List cards = new ArrayList<Card>();

        for (Card.Suit suit : Card.Suit.values()) {

            for (Card.Face face : Card.Face.values()) {
                cards.add(new Card(face.toString(), suit, 10));
            }

            for (int i = 2; i <= 10; i++) {
                cards.add(new Card(Integer.toString(i), suit, i));
            }

            // add the Ace
            cards.add(new Card(Card.ACE, suit, 1));
        }

        // shuffle cards and put them in a stack of cards
        Collections.shuffle(cards);
        cardStack.addAll(cards);
    }

    Integer cardsLeft() {
        return cardStack.size();
    }

    Card draw() {
        Card drawnCard = cardStack.poll();
        discardPile.add(drawnCard);

        // if there are no more cards left, we reshuffle the discarded pile use them again
        if (cardsLeft() == 0) {
            Collections.shuffle(discardPile);
            cardStack.addAll(discardPile);
            discardPile.clear();
        }

        return drawnCard;
    }
}
